import 'package:biofuture/screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:biofuture/screens/registration_screen.dart';
//import 'package:fluttertoast/fluttertoast.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  /*// form key
  final _formKey = GlobalKey<FormState>();

  // editing controller
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  // firebase
  final _auth = FirebaseAuth.instance;
  */

  //event input
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  //auth
  final FirebaseAuth auth = FirebaseAuth.instance;
  String _error = "";

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  Future<bool> _auth(email, password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        _error = 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        _error = 'Wrong password provided for that user.';
      }
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    // email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      /*validator: (value) {
        if (value!.isEmpty){
          return ("Please Enter Your Email");
        }
        // reg expression for email validation
        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
            .hasMatch(value)){
          return ("Please Enter A Valid Email");
        }
        return null;
      },
      onSaved: (value){
        emailController.text = value!;
      }, 
      textInputAction: TextInputAction.next,*/
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.mail),
        contentPadding: EdgeInsets.fromLTRB(20,15,20,15),
        hintText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          ),
      )
    );

    // password field
    final passwordField = TextFormField(
      autofocus: false,
      controller: passwordController,
      obscureText: true,
      /*
      validator: (value) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return("Password is required for login");
        }
        if(!regex.hasMatch(value)){
          return("Enter Valid Password(Min. 6 Character)");
        }
      },
      onSaved: (value){
        passwordController.text = value!;
      }, 
      textInputAction: TextInputAction.done,*/
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key),
          contentPadding: EdgeInsets.fromLTRB(20,15,20,15),
          hintText: "Password",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        )
      );

    // Syukri's
    final error = Text(_error, style: TextStyle(color: Colors.red));

    
    final loginButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.teal,
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20,15,20,15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () async {
          String email = emailController.text;
          String password = passwordController.text;
          print('email: ' + email);
          print('password: ' + password);

          if (await _auth(email, password)) {
            //empty error
            setState(() {
              _error = "";
            });

            //success
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
            );
          } else {
            //failure
            setState(() {
              _error = "password failure";
            });
          }
        },
        /*
        onPressed:(){
          signIn(emailController.text, passwordController.text);
          // Can use Navigator.pushReplacement also
          /*Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()));*/
        }, */
        child: Text(
          "Login", 
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      )),
    );
    
    /*
    final loginButton = ElevatedButton(
        onPressed: () async {
          String email = emailController.text;
          String password = passwordController.text;
          print('email: ' + email);
          print('password: ' + password);

          if (await _auth(email, password)) {
            //empty error
            setState(() {
              _error = "";
            });

            //success
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
            );
          } else {
            //failure
            setState(() {
              _error = "password failure";
            });
          }
        },
        child: const Text("Login"));
      */

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text("Login"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              //child: Form(
              child: Container(
                //key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                        height: 100,
                        child: Image.asset(
                          "assets/logo.png",
                          fit: BoxFit.contain
                          ),
                      ),
                    SizedBox(height: 65),
                    emailField,
                    SizedBox(height: 25),
                    passwordField, 
                    SizedBox(height: 25),   //height: 35
                    error,                  //syukri
                    SizedBox(height: 35),   //syukri
                    Padding(
                      padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
                      child: loginButton,
                    ),
                    SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account? "),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context, 
                                MaterialPageRoute(
                                    builder: (context) =>
                                        RegistrationScreen()));
                          },
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                                color: Colors.redAccent,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        )
                      ]
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

/*
// login function
void signIn(String email, String password) async{
  if(_formKey.currentState!.validate()){
    await _auth
      .signInWithEmailAndPassword(email: email, password: password)
      .then((uid) => {
        Fluttertoast.showToast(msg: "Login Successful"),
        //can use pushReplacement VV
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeScreen())),
      }).catchError((e)
      {
        Fluttertoast.showToast(msg: e!.message);
      });
  }
}*/
}